# blog-gitflow-maven
Código original: https://github.com/viesure/blog-gitflow-maven

precisa do git flow instalado
```
apt-get update && apt-get install -y git-flow
git flow init
```
### Exemplos
- mvn gitflow:release-start - Starts a release branch and updates version(s) to release version.
- mvn gitflow:release-finish - Merges a release branch and updates version(s) to next development version.
- mvn gitflow:release - Releases project w/o creating a release branch.
- mvn gitflow:feature-start - Starts a feature branch and optionally updates version(s).
- mvn gitflow:feature-finish - Merges a feature branch.
- mvn gitflow:hotfix-start - Starts a hotfix branch and updates version(s) to hotfix version.
- mvn gitflow:hotfix-finish - Merges a hotfix branch.
- mvn gitflow:support-start - Starts a support branch from the production tag.
- mvn gitflow:help - Displays help information.

Fonte: https://github.com/aleksandr-m/gitflow-maven-plugin



Exmaple project for the [Automating Semantig Versioning](https://viesure.io/automating-semantic-versioning-with-maven) blog post.

------

All commands are found in the `.gitlab-ci.yml` file.

You can test the different commands locally by simply executing them in any shell.
You don't need maven to try out the different commands, the repository comes with maven wrapper (just execute `./mvnw` instead).

The project can be taken and uploaded to Gitlab as is to test out the different pipeline jobs.
